**PAY ATTENTION:**  
**this upgrade [3.4.2] or HIGHER is MANDATORY and MUST BE APPLIED BEFORE 15/05/2024**
## [3.4.2] - 2024-01-25
### Added
- New verification CA chain management for license file
### Changed
- License CA verification management

# Time4ID SDK Android

Official repository for Time4ID android SDK, a simple mobile suite to use Time4Mind by android.

For integration info, please refer to the [project wiki](https://gitlab.com/intesi-group/time4id-sdk-android/-/wikis/home).

For technical API documentation, please refer [here](https://intesi-group.gitlab.io/time4id-sdk-android/).
