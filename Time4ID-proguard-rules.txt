# Add project specific ProGuard rules here.
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

-optimizations !field/removal/writeonly,!field/marking/private,!class/merging/*,!code/allocation/variable
-keepparameternames
-renamesourcefileattribute SourceFile
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,
                SourceFile,LineNumberTable,*Annotation*,EnclosingMethod

-keep public class * {
    public protected *;
}

# b/35135904 Ensure that proguard will not strip the mResultGuardian.

#-keep class com.google.android.gms.common.api.internal.BasePendingResult.ReleasableResultGuardian
#-keepclassmembers class com.google.android.gms.common.api.internal.BasePendingResult {
#  com.google.android.gms.common.api.internal.BasePendingResult.ReleasableResultGuardian *;
#}
-keep class com.google.android.gms.** { *; }
-dontwarn com.**

-keepclassmembernames class * {
    java.lang.Class class$(java.lang.String);
    java.lang.Class class$(java.lang.String, boolean);
}

-keepclasseswithmembernames,includedescriptorclasses class * {
    native <methods>;
}

-keepclassmembers,allowoptimization enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepnames public class * extends io.realm.RealmObject
-keep class com.intesigroup.time4eid.sdk.v2.models.T4Bitmap
-keep class io.realm.annotations.RealmModule
-keep class com.intesigroup.time4eid.sdk.v2.models.T4RealmTokenStore { public *; }
-keep class com.intesigroup.time4eid.sdk.v2.models.T4ContentResolver { public *; }
-keep @io.realm.annotations.RealmModule class *
-keep class io.realm.internal.Keep
-keep @io.realm.internal.Keep class * { *; }
-keep class com.intesigroup.time4eid.** { *; }
-keepnames class ext.org.bouncycastle.** { *; }
-keep class org.spongycastle.**
-keep class androidx.core.app.CoreComponentFactory

-keepnames class * implements java.io.Serializable

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    !private <fields>;
    !private <methods>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int d(...);
}

-dontwarn javax.**
-dontwarn io.realm.**