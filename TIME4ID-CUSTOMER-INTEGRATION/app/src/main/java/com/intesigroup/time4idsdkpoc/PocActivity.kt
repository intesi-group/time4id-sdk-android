package com.intesigroup.time4idsdkpoc

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.lifecycleScope
import com.google.gson.Gson
import com.intesigroup.time4eid.sdk.v2.enums.T4ProtectionType
import com.intesigroup.time4eid.sdk.v2.interfaces.T4OtpGenerationCallback
import com.intesigroup.time4eid.sdk.v2.models.T4Otp
import com.intesigroup.time4eid.sdk.v2.models.T4Token
import com.intesigroup.time4eid.sdk.v2.models.T4TokenId
import com.intesigroup.time4eid.sdk.v2.models.T4User
import com.intesigroup.time4idsdkpoc.ui.theme.Time4IdSDKPOCTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PocActivity : ComponentActivity() {

    companion object {
        const val TAG = "PocActivity"
        const val USERNAME = "username_here"
        const val PASSWORD = "password_here"
        const val JWT = "access_token_here"
    }

    enum class LoginType {
        USR_PSW,
        ACCESS_TOKEN
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Time4IdSDKPOCTheme {
                Column(
                    modifier = Modifier.fillMaxSize(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Text(text = stringResource(id = R.string.app_name))
                    Button(onClick = { generateOtpExample(LoginType.USR_PSW) }) {
                        Text(text = "Login with username and password")
                    }
                    Button(onClick = { generateOtpExample(LoginType.ACCESS_TOKEN) }) {
                        Text(text = "Login with access token")
                    }
                }
            }
        }
    }

    private fun generateOtpExample(loginType: LoginType) {
        lifecycleScope.launch(Dispatchers.IO) {
            //region 2. Login User
            login(loginType = loginType, onCompleted = {
                //region 3. Initialize Token
                initToken(onCompleted = { t4Token ->
                    //region 7. Generate OTP
                    generateOtp(t4Token, onOtpGenerated = { otp ->
                        Log.d(TAG, "onOtpGenerated: ${otp.otpValue}")
                        Toast
                            .makeText(
                                this@PocActivity,
                                "OTP:${otp.otpValue}",
                                Toast.LENGTH_SHORT
                            )
                            .show()
                    })
                    //endregion
                })
                //endregion
            })
        }
        //endregion
    }

    private fun login(loginType: LoginType, onCompleted: () -> Unit) {
        when (loginType) {
            LoginType.USR_PSW -> {
                //Use this function if you need to login with username and password
                loginWithUsernameAndPassword(onCompleted = onCompleted)
            }
            LoginType.ACCESS_TOKEN -> {
                //Or use this function if you need to login with accesstoken
                loginWithAccessToken(onCompleted = onCompleted)
            }
        }
    }

    private fun loginWithUsernameAndPassword(onCompleted: () -> Unit) {
        T4User.login(USERNAME, PASSWORD, false) { response ->
            if (response.hasError()) {
                Log.e(TAG, "Error: " + response.errorMessage)
            } else {
                onCompleted()
            }
        }
    }

    private fun loginWithAccessToken(onCompleted: () -> Unit) {
        T4User.login(JWT, true) { response ->
            if (response.hasError()) {
                Log.e(TAG, "Error: " + response.errorMessage)
            } else {
                onCompleted()
            }
        }
    }

    private fun initToken(onCompleted: (t4Token: T4Token) -> Unit) {
        T4Token.initToken(null, null) { response, respObj ->
            if (response.hasError()) {
                Log.e(TAG, "Error: " + response.errorMessage)
            } else {
                //region 4. Fetch Token
                val tokenId = respObj as T4TokenId
                val t4Token = fetchToken(tokenId)
                //endregion
                //region 5. Generate Seed
                generateSeed(t4Token, onSeedGenerated = {
                    //region 6. Activate Token
                    activate(t4Token, onTokenActivated = {
                        onCompleted(t4Token)
                    })
                    //endregion
                })
                //endregion
            }
        }
    }

    private fun fetchToken(tokenId: T4TokenId) = T4Token.find(tokenId)

    private fun generateSeed(t4Token: T4Token, onSeedGenerated: () -> Unit) {
        t4Token.getSeed(
            null, T4ProtectionType.T4_PROTECTION_NONE, null, false, this
        ) { response ->
            if (response.hasError()) {
                Log.e(TAG, "Error: " + response.errorMessage)
            } else {
                Log.d(TAG, "getSeed: ${Gson().toJson(response)}")
                onSeedGenerated()
            }
        }
    }

    private fun activate(t4Token: T4Token, onTokenActivated: () -> Unit) {
        t4Token.activate(null, this) { response ->
            if (response.hasError()) {
                Log.e(TAG, "Error: " + response.errorMessage)
            } else {
                onTokenActivated()
            }
        }
    }

    private fun generateOtp(t4Token: T4Token, onOtpGenerated: (otp: T4Otp) -> Unit) {
        t4Token.generateOtp(null, this, object : T4OtpGenerationCallback {
            override fun onOtpGenerated(otp: T4Otp, pinValue: String?) {
                onOtpGenerated(otp)
            }

            override fun onError(errorCode: Int, errorMessage: String) {
                Log.d(TAG, "Error: $errorMessage")
            }
        })
    }
}