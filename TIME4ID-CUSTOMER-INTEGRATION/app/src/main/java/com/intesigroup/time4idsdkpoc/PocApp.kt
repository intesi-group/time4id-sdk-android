package com.intesigroup.time4idsdkpoc

import android.app.Application
import com.intesigroup.time4eid.sdk.v2.T4ID

class PocApp : Application() {

    companion object {
        const val LICENSE_FILE = "license_file_name_here.txt.p7m"
    }

    override fun onCreate() {
        super.onCreate()
        // region 1. Setup Time4ID SDK
        T4ID.init(LICENSE_FILE, null, applicationContext)
        //endregion
    }
}