# Changelog
All notable changes to this project will be documented in this file.

## [3.5.14] - 2024-12-06
### Changed
- Fixed nextOtpAvailableTime method, in order to have the next available window to use OTP in all the generation otp flows
- getNextOtpAvailableTime methods documentation, fit the current implementation


## [3.5.12] - 2024-09-05
### Changed
- getQRCertificate API rework: now in login phase retrieve the QR code certificate and store it in the local DB only if there isn't a certificate saved.
- parseDynamicLinkMessage API rework: After a QRcode was scanned, if no cert is present in the DB or the cert is not valid, the getQRCertificate API will be called to retrieve the cert.

## [3.5.0] - 2024-05-06
### Added
- MASVS security issues fixed

## [3.4.3] - 2024-02-28
### Changed
- sourceCompatibility compileOptions downgraded from Java17 to Java11
- sourceCompatibility compileOptions downgraded Kotlin plugin from 1.8.21 to 1.7.0

**PAY ATTENTION:**  
**this upgrade [3.4.2] or HIGHER is MANDATORY and MUST BE APPLIED BEFORE 15/05/2024**

## [3.4.2] - 2024-01-25
### Added
- New verification CA chain management for license file
### Changed
- License CA verification management

## [3.3.2] - 2023-09-26
### Added
- Init SDK with license by byte array with:
```kotlin
T4ID.init(appContext, byteArray, providersList)
```
### Changed
- Compile with Android targetSdk API 34

## [3.2.6] - 2023-09-09
### Changed
- Gradle 8.3 support
- Upgraded realm database to 10.16.1

## [3.1.0] - 2023-03-21
### Added
- JWT Login (check the updated sample project!)
```kotlin
T4User.login(JWT, true) { response -> {...} }
```

## [3.0.0] - 2023-03-15
### Changed
- New binary managemement system, in order to build library

## [2.23.1] - 2022-11-30
### Changed
- Gradle deps upgraded

## [2.23.0] - 2022-08-31
### Added
- SSL EndToEnd encryption certs management added: multi certs management
  
## [2.21.03] - 2021-10-20
### Changed
- Check URL validity is not mandatory into qrcode scan
- SAST compliance

## [2.21.01] - 2021-09-20
### Changed
- isSyncronized API has now return NO_ERROR, with message: "already synchronized" in case of no operation to do found

## [2.21.00] - 2021-08-04
### Changed
- RealmDB plugin upgraded to v10.6.0: operation does NOT permit downgrade to old versions

## [2.20.00] - 2021-06-22
### Removed
- Removed antifraud management
### Changed
Improve login networking management

## [2.19.22] - 2021-04-15
### Added
- added sharedTokens migration (to FALSE) management
### Changed
- T4Token.enrollForService() ehancement: is now possible to restore a previous token enrollment locally, with "plus" button
- Fixed a bug with username containing special chars: fixed the escaping management and the DB store
- Fixed touchID token protection from getServiceCompanyInfo() BE management

## [2.19.21] - 2021-03-26
### Added
- Added TOKEN_EXPIRED constants with integer 13056, in order to handle by the integrator the token expired events
### Changed
- Fixed android 5 support for old webView management
- Minor fix for getTokenStatus API management

## [2.19.19] - 2021-01-25
### Changed
- Check if generated OTP is empty
